﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using XUnitTestingTestverktygLektion7;

namespace XUnitTestingTestverktygLektion7_Test
{
    public class NameTest
    {
        [Fact]
        public void GetFullName_Joe_Lowell()
        {
            Name name = new Name();
            name.firstName = "Joe";
            name.lastName = "Lowell";
            string expected = "Joe Lowell";

            string actual = name.GetFullName();

            Assert.Equal(expected, actual, ignoreCase:true);
            Assert.Contains("oe", actual, StringComparison.InvariantCultureIgnoreCase);
            Assert.StartsWith("jo", actual, StringComparison.InvariantCultureIgnoreCase);
            Assert.EndsWith("ell", actual, StringComparison.InvariantCultureIgnoreCase);
            Assert.Matches("[A-Z]{1}[a-z]+ [A-Z]{1}[a-z]+", actual);
        }

        [Fact]
        public void Name_ShouldBeNull_IfWeHaventAddedAny()
        {
            Name name = new Name();
            name.setFirstName("");
            Assert.Null(name.firstName);
        }

        [Fact]
        public void Name_ShouldNotBeNull_IfWeAdded()
        {
            Name name = new Name();
            name.setFirstName("George");
            Assert.NotNull(name.firstName);
        }

        [Fact]
        public void setFirstNameThrowsError_WhenEmptyString()
        {
            Name name = new Name();
            Assert.Throws<ArgumentException>(
                () => name.setFirstNameThrowsError("")
            );
        }

        [Fact]
        public void NameFactory_ShouldGetPersonWhenGivingSocialSecurityNumber()
        {
            Name name = new Name();

            Name newName = name.NameFactory("324567");
            Assert.IsType<Person>(newName);
        }
    }
}
