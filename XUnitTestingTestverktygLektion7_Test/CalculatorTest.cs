using System;
using Xunit;
using XUnitTestingTestverktygLektion7;

namespace XUnitTestingTestverktygLektion7_Test
{
    public class CalculatorTest
    {
        [Fact]
        public void Add_10and5_Get15()
        {
            /*Arrange - Given*/
            Calculator calculator = new Calculator();
            int value1 = 5;
            int value2 = 11;
            int expected = 15;

            /*Act - When*/
            int actual = calculator.Add(value1, value2);
            
            /*Assert - Then*/
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Add_4p5and5p2_Get9p7()
        {
            /*Arrange - Given*/
            Calculator calculator = new Calculator();
            double value1 = 4.55;
            double value2 = 5.22;
            double expected = 9.8;

            /*Act - When*/
            double actual = calculator.Add(value1, value2);

            /*Assert - Then*/
            Assert.Equal(expected, actual, 1);
        }

        [Fact]
        public void FibonacciDoesNotHaveA4()
        {
            Calculator calculator = new Calculator();
            
            Assert.All(calculator.Fibonacci, 
                number => 
                {
                    Assert.NotEqual(0, number);
                    Assert.NotEqual(4, number);
                    
                    });
            
            foreach(int number in calculator.Fibonacci)
            {
                Assert.NotEqual(4, number);
            }
        }

        [Fact]
        public void FibonacciDoesNotHaveA4Version2()
        {
            Calculator calculator = new Calculator();

            Assert.DoesNotContain(4, calculator.Fibonacci);
        }

        [Fact]
        public void FibonacciDoesHave13()
        {
            Calculator calculator = new Calculator();

            Assert.Contains(13, calculator.Fibonacci);
        }

        [Fact]
        public void FirstFibonacciBetween0and3()
        {
            Calculator calculator = new Calculator();
            int actual = calculator.Fibonacci[0];
            Assert.InRange(actual, 0, 3);
        }
    }
}
